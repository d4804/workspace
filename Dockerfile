# inspired by https://github.com/eriksoderblom/alpine-apache-php
# docker build -t registry.gitlab.rlp.net/studiengang-digitale-methodik/modul-7/7b/workspace/sturm-app .
# docker push registry.gitlab.rlp.net/studiengang-digitale-methodik/modul-7/7b/workspace/sturm-app
# docker-compose up -d

FROM alpine:3.15

# Setup apache and php
RUN apk --update \
    add apache2 \
    curl \
    git \
    vim \
    zip \
    unzip \
    php8-apache2 \
    php8-bcmath \
    php8-bz2 \
    php8-calendar \
    php8-common \
    php8-cgi \
    php8-ctype \
    php8-curl \
    php8-dom \
    php8-gd \
    php8-iconv \
    php8-json \
    php8-mbstring \
    php8-openssl \
    php8-pdo \
    php8-pdo_sqlite \
    php8-phar \
    php8-posix \
    php8-session \
    php8-sockets \
    php8-tokenizer \
    php8-simplexml \
    php8-xmlwriter \
    php8-xml \
    php8-xsl \
    php8-zip \
    && cd /usr/bin && ln -s php8 php \
    && rm -f /var/cache/apk/* \
    && mkdir -p /opt/utils \
    && mkdir /htdocs

EXPOSE 80

ADD files/container/start.sh /opt/utils/

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

RUN curl -O https://php-school.github.io/workshop-manager/workshop-manager.phar
RUN mv workshop-manager.phar /usr/local/bin/workshop-manager
RUN chmod +x /usr/local/bin/workshop-manager
ENV PATH="$PATH:/root/.php-school/bin"
RUN workshop-manager install learnyouphp -v
RUN apk add ncurses

RUN chmod +x /opt/utils/start.sh
RUN /opt/utils/start.sh

HEALTHCHECK CMD wget -q --no-cache --spider localhost/index.html

ENTRYPOINT ["httpd","-D","FOREGROUND"]

VOLUME /htdocs
