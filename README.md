#### Studiengang Digitale Methodik in den Geistes- und Kulturwissenschaften

### Open Educational Resources

## Code der Beispiel-Webanwendung zur Übung

Research Software Engineering: [Torsten Schrade](https://orcid.org/0000-0002-0953-2818)

Dieses Repositorium enthält die Beispiel-Webanwendung zur Übung [Methodenpraxis für die Modellierung, Gestaltung und Entwicklung von geisteswissenschaftlichen Webanwendungen](https://studiengang-digitale-methodik.pages.gitlab.rlp.net/modul-7/7b/pages/) mit allen Musterlösungen.

Ein fertiges Docker Image mit der Beispiel-Webanwendung findet sich in der Container Registry:

https://gitlab.rlp.net/studiengang-digitale-methodik/modul-7/7b/workspace/container_registry

Alle Details zur Installation finden sich im [Übungshandbuch](https://studiengang-digitale-methodik.pages.gitlab.rlp.net/modul-7/7b/pages/tools/).

**Quickstart:**

1. Repository klonen:

```bash
git clone git@gitlab.rlp.net:studiengang-digitale-methodik/modul-7/7b/workspace.git
```

2. In das Repository wechseln und Branches mit den Musterlösungen holen:

```bash
git branch -a | sed -n "/\/HEAD /d; /\/master$/d; /remotes/p;" | xargs -L1 git checkout -t
```

3. In das Verzeichnis oberhalb des Repositorys wechseln und Docker Container starten:

```bash
docker-compose up -d
```

4. Um die vollständige Musterversion der App laufen zu lassen

In den laufenden Docker Container wechseln:

```bash
docker exec -it sturm-app /bin/sh
```

Dort in das Verzeichnis /htdocs/ wechseln, den Branch mit dem Gesamtcode auschecken und per Composer installieren

```bash
cd /htdocs/
git checkout php10 && composer install
```

5. Browser öffnen und http://localhost:8090/public/ aufrufen

Diese Beispielanwendung steht unter MIT Lizenz.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
